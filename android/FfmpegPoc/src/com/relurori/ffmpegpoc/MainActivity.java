package com.relurori.ffmpegpoc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import android.graphics.ImageFormat;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PictureCallback;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.ShutterCallback;
import android.hardware.Camera.Size;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.ParcelFileDescriptor;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.SurfaceHolder.Callback;
import android.view.View.OnTouchListener;
import android.view.SurfaceView;
import android.widget.Button;
import android.widget.Toast;



public class MainActivity extends Activity {


	public Button bStart;
	public Button bStop;
	SurfaceHolder mHolder;
	SurfaceView mSurfaceView;
	CaptureVideoView mCamView;
	CaptureAudio mCapAudio;
	
	private String TAG = "mylib";
	private String localPath;
	
	//SocketProxy mSockProx;
    String ip = "192.168.0.109";
    int videoPort = 34567;
    int audioPort = 45678;
    
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		File sdpath = Environment.getExternalStorageDirectory();
        localPath = sdpath.toString() + "/dummy.mp4";
        
        /*
        mSockProx = new SocketProxy("relurori.ffmpegtest");
        */
        
		mSurfaceView = (SurfaceView)findViewById(R.id.surfaceView1);
		/* custom view; isn't it cool? */
		mCamView = (CaptureVideoView) findViewById(R.id.camview);
		mCamView.SetupCamera(mSurfaceView);
		
		mCapAudio = new CaptureAudio();

		onCreateButtonSetup();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
    
    private void onCreateButtonSetup() {
    	bStart = (Button) findViewById(R.id.buttonStart);
    	bStop = (Button) findViewById(R.id.buttonStop);
    	
    	bStart.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				mSockProx.SetupSocketProxy();
				FileDescriptor fd = mSockProx.GetSendFileDescriptor();
				*/
				
				try {
					mCamView.setNetworkDestination(InetAddress.getByName(ip), videoPort);
					mCapAudio.setNetworkDestination(InetAddress.getByName(ip), audioPort);
				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				try {
					mCamView.SetupMediaRecorderNetwork(1024, 768);
					mCapAudio.SetupMediaRecorderNetwork();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				mCamView.StartRecording();
				mCapAudio.StartRecording();
			}
		});
    	bStop.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				/*
				 * mSockProx.DestroySocketProxy();
				 */
				mCamView.StopRecording();
				mCapAudio.StopRecording();

			}
    	});
    }

}