package com.relurori.ffmpegpoc;

import java.io.IOException;
import java.net.InetAddress;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/*
 * Decoupled from the video capturing class. Still need to create an abstract
 * class if we want to add more codecs to support. Otherwise, this is _okay_,
 * but not exactly OOP-ly of us. So many shared functions. 
 */
public class CaptureAudio {

	private String TAG = "CaptureAudio";
	private String LOCAL_SOCKET_NAME = "com.relurori.ffmpegpoc.audio";
	private int RECV_BUFFER_SIZE = 234567;
	private int SEND_BUFFER_SIZE = 234567;
	
	private MediaRecorder mMediaRecorder;
	
	// streaming
	private LocalSocket mRecv;
	private LocalSocket mSend;
	private LocalServerSocket mServerSocket;
	private AbstractPacketer mPacketer;

	private boolean bStartRecording = false;
	
	public CaptureAudio() {

		try {
			mServerSocket = new LocalServerSocket(LOCAL_SOCKET_NAME);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			mPacketer = new AMRPacketer();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void setNetworkDestination(InetAddress dst, int dport) {
		mPacketer.SetDestination(dst, dport);
	}
	
	private void SetupMediaRecorder() {
		// TODO Auto-generated method stub

		mMediaRecorder = new MediaRecorder();
		/* Use a separate MediaRecorder instance for the audio. */
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		// TODO wtf is RAW_AMR and how does it differ?
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mMediaRecorder.setAudioChannels(1);
	}
	
	public void SetupMediaRecorderNetwork() throws IOException {
		SetupMediaRecorder();
		startNetwork();
		
		mPacketer.setInputStream(mRecv.getInputStream());		
		mPacketer.start();
		Log.e(TAG, "Packeter started packeting");
		
		mMediaRecorder.setOutputFile(mSend.getFileDescriptor());
	}

	public boolean StartRecording() {
		try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d(TAG, "IllegalStateException: " + e.getMessage());
			e.printStackTrace();
			destroyMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d(TAG, "IOException: " + e.getMessage());
			e.printStackTrace();
			destroyMediaRecorder();
			return false;
		}
		
		Log.e(TAG, "StartRecording: starting");
		
		mMediaRecorder.start();
		return true;
	}	
	
	private void startNetwork() {
		// TODO Auto-generated method stub
		
		mRecv = new LocalSocket();
		try {
			mRecv.connect(new LocalSocketAddress(LOCAL_SOCKET_NAME));
			mRecv.setReceiveBufferSize(RECV_BUFFER_SIZE);
			mSend = mServerSocket.accept();
			mSend.setSendBufferSize(SEND_BUFFER_SIZE);
			mPacketer.setInputStream(mRecv.getInputStream());
			
			Log.e(TAG, "startNetwork: complete");
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			StopRecording();
		}

	}

    /* TODO this is probably broken */
	private void stopNetwork() {
		try {
			mSend.close();
			mRecv.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    /* TODO this is probably broken */
	private void destroyNetwork() {
		stopNetwork();
		
		try {
			mServerSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    /* TODO this is probably broken */
	public void StopRecording() {
		if (bStartRecording)
			return;
		/* must be called in order */
		mMediaRecorder.stop();
		destroyMediaRecorder();
		
		destroyNetwork();
	}
	
    /* TODO this is probably broken */
	private void destroyMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset();
			mMediaRecorder.release();
		}
		mMediaRecorder = null;
		bStartRecording = false;
	}
	
}
