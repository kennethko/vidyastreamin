package com.relurori.ffmpegpoc;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.SocketException;
import java.util.Random;

import android.util.Log;

public class RtpSocket extends PacketDissect {

	private String TAG = "mylib";
	
	public static int RTP_HEADER_LENGTH = 12;
	public static int MTU = 1500;
	
	private byte[] mBuffer = new byte[MTU];
	private long mSeqNo = 0;
	private int mSsrc;
	private int mDstPort;
	private boolean setNextPacketRTPMarker = false;
	
	/* UDP for the RTP packets */
	private DatagramPacket mPacket;
	/* Multicast Streaming: many viewers, less resources used.
	 * Downside is that routing configuration may not be playing
	 * nicely with multicast.
	 */
	private MulticastSocket mSocket;
    /* Because unicast is feasible for this, too. */
    //private DatagramSocket mSocket;
	
	/*
	    0                   1                   2                   3
	    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |V=2|P|X|  CC   |M|     PT      |       sequence number         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                           timestamp                           |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |           synchronization source (SSRC) identifier            |
	   +=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
	   |            contributing source (CSRC) identifiers             |
	   |                             ....                              |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	
   		Figure 1.  RTP header according to RFC 3550
	*/
	
	public RtpSocket() throws IOException {
		/*	
		     0 1 2 3 4 5 6 7
		    +-+-+-+-+-+-+-+-+
		 	|V=2|P|X|  CC   |
		 	+---------------+
		 	
		 	version (V): 2 bits
		 	
			This field identifies the version of RTP.  The version defined by
			this specification is two (2).  (The value 1 is used by the first
			draft version of RTP and the value 0 is used by the protocol
			initially implemented in the "vat" audio tool.) (RFC 3550)
		 
		 */
        mBuffer[0] = (byte) 0x80;

		/*
			In addition, payload type
				values in the range 96-127 MAY be defined dynamically through a
				conference control protocol, which is beyond the scope of this
				document. (RFC 3550, RFC 3551)
			
			Payload identifiers 96–127 are reserved for payloads defined 
			dynamically during a session.
			(http://en.wikipedia.org/wiki/RTP_audio_video_profile)
		 */
		mBuffer[1] = 96;
		// SN, TS, SSRC. whatevs.
		Random rand = new Random();
		int ssrc = rand.nextInt();
		setLong(mBuffer, 8, 12, ssrc);
		
		mPacket = new DatagramPacket(mBuffer, 1);
		mSocket = new MulticastSocket();
        //mSocket = new DatagramSocket();
	}
	
	public byte[] getBuffer() {
		return mBuffer;
	}
	
	public void SetDestination(InetAddress dest, int dport) {
		mDstPort = dport;
		
		mPacket.setPort(mDstPort);
		mPacket.setAddress(dest);
		
		Log.e(TAG, "SetDestination port=" + mDstPort + " address=" + dest.getHostAddress());
	}
	
	public void updateTimestamp(long ts) {
		setLong(mBuffer, 4, 8, ts);
	}
	

	/*
		marker (M): 1 bit
			The interpretation of the marker is defined by a profile.  It is
			intended to allow significant events such as frame boundaries to
			be marked in the packet stream.  A profile MAY define additional
			marker bits or specify that there is no marker bit by changing the
			number of bits in the payload type field (see Section 5.3).
			(RFC 3550)
			
		0x80 == 1000 0000
	 */
	public void setNextPacketRTPMarker() {
		
		setNextPacketRTPMarker = true;
		mBuffer[1] += 0x80;
	}

	/*
        marker (M): 1 bit
            The interpretation of the marker is defined by a profile.  It is
            intended to allow significant events such as frame boundaries to
            be marked in the packet stream.  A profile MAY define additional
            marker bits or specify that there is no marker bit by changing the
            number of bits in the payload type field (see Section 5.3).
            (RFC 3550)
            
        0x80 == 1000 0000
    */
	private void clearNextPacketRTPMarker() {
		if (setNextPacketRTPMarker) {
			setNextPacketRTPMarker = false;
			mBuffer[1] -= 0x80;
		}
	}
	
    /** 
     * incrementSequenceNo - bump up the sequence number
     *
     * super joke function.
     */
	private void incrementSequenceNo() {
		// TODO Auto-generated method stub
		mSeqNo++;
		setLong(mBuffer, 2, 4, mSeqNo);
	}
	
	public void send(int len) throws IOException {
		incrementSequenceNo();
		
		//Log.e(TAG, "RtpSocket: want to send " + len + " bytes");
		
		mPacket.setLength(len);
		mSocket.send(mPacket);
		
		//Log.e(TAG, "RtpSocket: sent " + len + " bytes");
		
		clearNextPacketRTPMarker();
	}

	public int getDestinationPort() {
		return mDstPort;
	}
}
