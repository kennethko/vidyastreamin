package com.relurori.ffmpegpoc;

import java.io.IOException;

import android.os.SystemClock;
import android.util.Log;

public class AMRPacketer extends AbstractPacketer implements Runnable {

	private String TAG = "AMRPacketer";
	private long mDelay;
	
	private Thread mThread;
	private PacketStatistics mPackStats = new PacketStatistics();
	
	public AMRPacketer() throws IOException {
		super();
		// TODO Auto-generated constructor stub
		Log.e(TAG, "here i am");
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		long delta = 0;
		long startTime = 0;
		
		while (true) {
			
			break;
		}
		
		try {
			while (!Thread.interrupted()) {
				/* estimate delay. 
				 * network activity probably slower than cpu */
				startTime = SystemClock.elapsedRealtime();
	
					send();
				
				delta = SystemClock.elapsedRealtime() - startTime;
				mPackStats.push(delta);
				mDelay = mPackStats.average();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		if (mThread == null) {
			mThread = new Thread(this);
			mThread.start();
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		try {
			mInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		mThread.interrupt();
		try {
			mThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mThread = null;
	}

	
    /**
     * fillBuffer - fill buffer with contents read off mInputStream
     *
     * Useful for skipping the header of an encapsulation. 
     * Think about it.
     */
	private int fillBuffer(int offset, int length) throws IOException {
		int sum = 0;
		int len = 0;
		
		while (sum < length) {
			len = mInputStream.read(mBuffer, offset + sum, length - sum);
			if (len < 0) {
                // End of stream !!!
				throw new IOException("EOS");
			} else {
				sum += len;
			}
		}
		return sum;
	}
	
	
	private void send() throws IOException {
		// TODO Auto-generated method stub
		
		// arbitrary amount
		fillBuffer(mRtphl, 50);
		
		// FOR DEBUG/DEV ONLY LOL
		//Log.e(TAG, bytesToHex(mBuffer));
	}
}
