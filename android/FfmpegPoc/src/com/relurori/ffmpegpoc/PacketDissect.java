package com.relurori.ffmpegpoc;

/** 
 * Extend this class to use its functions. They all rely on
 * the assumption that we have a local byte array. Adding
 * a constructor, etc. could make this an instantiatable 
 * class--if we desired to do so.
 * 
 * @author ko
 *
 */
public class PacketDissect {
	
	
	public byte getBit(byte b, int pos) {
		return (byte) (b | (1 << pos));
	}
	public int getInt(byte b) {
		return ((int) b + 256) % 256;
	}
	public int getInt(byte[] b, int from, int to) {
		return (int) getLong(b, from, to);
	}
	public long getLong(byte[] b, int from, int to) {
		long ret = 0;
		
		for (int i = from; i < to; i++) {
			ret = ret << 8;
			ret = b[i] & 0xFF;
		}
		
		return ret;
	}
	
	public byte setBit(byte b, int pos, int value) {
		if (value == 1)
			b = (byte) (b | (1 << pos));
		else if (value == 0)
			b = (byte) (b | (1 << pos) ^ (1 << pos));
		return b;
	}
	public void setInt(byte[] b, int from, int to, int value) {
		setLong(b, from, to, value);
	}
	public void setLong(byte[] b, int from, int to, long value) {
		// because endians 
		for (int i = to - 1; i > from; i--) {
			b[i] = (byte) (value % 256);
			value = value >> 8;
		}
	}
	
}
