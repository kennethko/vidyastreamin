package com.relurori.ffmpegpoc;

import java.io.File;
import java.io.FileDescriptor;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import android.content.Context;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.ParcelFileDescriptor;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

/*
 * There is a _damn_ good chance that this class is eventually going to
 * be broken up and dismantled in an ugly fashion. When creating it there 
 * were two reasons for its inception:
 *
 *  1.  Creating a custom view seemed like a cool idea. 
 *      a.  It definitely is cool.
 *  2.  Did not realize that MediaRecorder would require multiple instances.
 *      At least, with the way the project is going. The rationale was to
 *      just have a single h.264/amr blob and RTP-friendlicate it.   
 *
 * Reasons for the breakup is that MediaRecorder needs multiple streams--one
 * per track (or so it seems). That is the current working assumption. Next
 * is that this is not OOP-ly. At all. Here we're stuck with using H.264
 * and AMR as our codecs, but what if we don't want to be limited? Abstract
 * all the classes? There is no AbstractStreamer class. No H264Streamer 
 * extending the AbstractStreamer class. No OOP design, really. 
 */
public class CaptureVideoView extends View implements SurfaceHolder.Callback, View.OnTouchListener {
	
	private String TAG = "CaptureVideoView";
	private String LOCAL_SOCKET_NAME = "com.relurori.ffmpegpoc.video";
	private int RECV_BUFFER_SIZE = 234567;
	private int SEND_BUFFER_SIZE = 234567;
	
	public Camera mCamera;
	private MediaRecorder mMediaRecorder;
	private SurfaceView mSurfaceView;
	private SurfaceHolder mHolder;
	
	// streaming
	private LocalSocket mRecv;
	private LocalSocket mSend;
	private LocalServerSocket mServerSocket;
	private AbstractPacketer mPacketer;
	
	private boolean bStartRecording = false;

	public CaptureVideoView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// TODO Auto-generated constructor stub
		
		try {
			mServerSocket = new LocalServerSocket(LOCAL_SOCKET_NAME);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			mPacketer = new H264Packeter();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Log.e(TAG, "here i am");
	}
	
	/* Streaming */
	public void setNetworkDestination(InetAddress dst, int dport) {
		mPacketer.SetDestination(dst, dport);
	}
	
	public int getNetworkDestinationPort() {
		return mPacketer.getRtpSocket().getDestinationPort();
	}

	
	/* Camera */
	public void SetupCamera(SurfaceView sv) {
		// TODO Auto-generated method stub
			
    	mSurfaceView = sv;
    	mHolder = mSurfaceView.getHolder();
		mHolder.addCallback(this);
		
		mCamera = Camera.open();
		setOnTouchListener(this);
	}
	
	private void SetupMediaRecorder() {
		// TODO Auto-generated method stub

		if (mCamera == null) {
			Log.e(TAG, "camera null during setup");
			return;
		}
		
		mMediaRecorder = new MediaRecorder();
		mCamera.unlock();
		mMediaRecorder.setCamera(mCamera);
		/* Use a separate MediaRecorder instance for the audio. */
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
		mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
		mMediaRecorder.setVideoSize(1280, 720);
		mMediaRecorder.setVideoFrameRate(30);
		mMediaRecorder.setPreviewDisplay(mHolder.getSurface());
	}
	
	public void SetupMediaRecorderLocal(String filename, int width, int height) {
		SetupMediaRecorder();
		mMediaRecorder.setOutputFile(filename);
		return;
	}
	
	public void SetupMediaRecorderNetwork(int width, int height) throws IOException {
		SetupMediaRecorder();
		startNetwork();
		
		mPacketer.setInputStream(mRecv.getInputStream());		
		mPacketer.start();
		Log.e(TAG, "Packeter started packeting");
		
		mMediaRecorder.setOutputFile(mSend.getFileDescriptor());
	}
	
	public boolean StartRecording() {
		if (mCamera == null) {
			Log.d(TAG, "StartRecording: mCamera == null");
			return false;
		}
		
		try {
			mMediaRecorder.prepare();
		} catch (IllegalStateException e) {
			Log.d(TAG, "IllegalStateException: " + e.getMessage());
			e.printStackTrace();
			destroyMediaRecorder();
			return false;
		} catch (IOException e) {
			Log.d(TAG, "IOException: " + e.getMessage());
			e.printStackTrace();
			destroyMediaRecorder();
			return false;
		}
		
		Log.e(TAG, "StartRecording: starting");
		
		mMediaRecorder.start();
		return true;
	}	
	
	private void startNetwork() {
		// TODO Auto-generated method stub
		
		mRecv = new LocalSocket();
		try {
			mRecv.connect(new LocalSocketAddress(LOCAL_SOCKET_NAME));
			mRecv.setReceiveBufferSize(RECV_BUFFER_SIZE);
			mSend = mServerSocket.accept();
			mSend.setSendBufferSize(SEND_BUFFER_SIZE);
			mPacketer.setInputStream(mRecv.getInputStream());
			
			Log.e(TAG, "startNetwork: complete");
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			StopRecording();
		}

	}
	
    /* TODO this is probably broken */
	private void stopNetwork() {
		try {
			mSend.close();
			mRecv.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    /* TODO this is probably broken */
	private void destroyNetwork() {
		stopNetwork();
		
		try {
			mServerSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    /* TODO this is probably broken */
	public void StopRecording() {
		if (mCamera == null || bStartRecording)
			return;
		/* must be called in order */
		mMediaRecorder.stop();
		destroyMediaRecorder();
		
		destroyNetwork();
	}
	
    /* TODO this is probably broken */
	private void destroyMediaRecorder() {
		if (mMediaRecorder != null) {
			mMediaRecorder.reset();
			mMediaRecorder.release();
		}
		mMediaRecorder = null;
		mCamera = null;
		bStartRecording = false;
	}
	
	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		return false;
	}

}
