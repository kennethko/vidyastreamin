package com.relurori.ffmpegpoc;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;

public abstract class AbstractPacketer {
	
	protected static final int mRtphl = RtpSocket.RTP_HEADER_LENGTH;
	
	protected RtpSocket mSock;
	protected InputStream mInputStream;
	
	protected boolean mRunning = false;
	protected byte[] mBuffer;

	public abstract void start();
	public abstract void stop();
	
	public AbstractPacketer() throws IOException {
		mSock = new RtpSocket();
		mBuffer = mSock.getBuffer();
	}
	public AbstractPacketer(InputStream inStream) {
		super();
		mInputStream = inStream;
	}
	
	public RtpSocket getRtpSocket() {
		return mSock;
	}
	
	public void SetDestination(InetAddress dest, int dport) {
		mSock.SetDestination(dest, dport);
	}
	
	public void setInputStream(InputStream inputStream) {
		mInputStream = inputStream;
	}
	
	/* 
	 * byte[] to hex string.
	 * Because having this in the parent is still useful.
	 * 
	 * Source: http://stackoverflow.com/questions/9655181/convert-from-byte-array-to-hex-string-in-java
	 */
	public static String bytesToHex(byte[] bytes) {
	    final char[] hexArray = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
	    char[] hexChars = new char[bytes.length * 2];
	    int v;
	    for ( int j = 0; j < bytes.length; j++ ) {
	        v = bytes[j] & 0xFF;
	        hexChars[j * 2] = hexArray[v >>> 4];
	        hexChars[j * 2 + 1] = hexArray[v & 0x0F];
	    }
	    return new String(hexChars);
	}
}
