package com.relurori.ffmpegpoc;

import java.io.IOException;

import android.os.SystemClock;
import android.util.Log;

/*
 * TODO
 * o	timestamp in the FU-A creation path 
 * 		o	may not be updating properly. maybe too tired.
 */
public class H264Packeter extends AbstractPacketer implements Runnable {

	public String TAG = "H264Packeter";
	
	private int MAX_PKT_SIZE = 1400;
    private int FU_HEADER_SIZE = 1;
    private int FU_INDICATOR_SIZE = 1;
    private int FU_A_HEADER_SIZE = FU_HEADER_SIZE + FU_HEADER_SIZE;
    
	private int mNaluLength;
	private long mTimestamp = 0;
	private long mDelay;

	private Thread mThread;
	private PacketStatistics mPackStats = new PacketStatistics();
	
	public H264Packeter() throws IOException {
		super();
		Log.e(TAG, "here i am");
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		long delta = 0;
		long startTime = 0;
		
		try {
            /* read 4 at a time because we're looking for the mdat atom */
			byte buffer[] = new byte[4];
			while (true) {
				
				/* mdat atom is where actual raw file info is 
				 * so let's wait for that to get past it. 
				 * 
				 * let's poll on an 'm' first. */
				while (mInputStream.read() != 'm') {}
				
				/* we got an 'm'. let's try our luck with the
				 * next 3 characters: dat cool? funny. */
				mInputStream.read(buffer, 0, 3);
				if (buffer[0] != 'd')
					continue;
				if (buffer[1] != 'a')
					continue;
				if (buffer[2] != 't')
					continue;
				
				Log.e(TAG, "H264: MDAT found");
				
				/* apparently we passed the checks. found
				 * the mdat portion */
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
		
		try {
			while (!Thread.interrupted()) {
				/* estimate delay. 
				 * network activity probably slower than cpu */
				startTime = SystemClock.elapsedRealtime();
				send();
				delta = SystemClock.elapsedRealtime() - startTime;
				mPackStats.push(delta);
				mDelay = mPackStats.average();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * fillBuffer - fill buffer with contents read off mInputStream
     *
     * Useful for skipping the header of an encapsulation. 
     * Think about it.
     */
	private int fillBuffer(int offset, int length) throws IOException {
		int sum = 0;
		int len = 0;
		
		while (sum < length) {
			len = mInputStream.read(mBuffer, offset + sum, length - sum);
			if (len < 0) {
                // End of stream !!!
				throw new IOException("EOS");
			} else {
				sum += len;
			}
		}
		return sum;
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		if (mThread == null) {
			mThread = new Thread(this);
			mThread.start();
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		try {
			mInputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		mThread.interrupt();
		try {
			mThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		mThread = null;
	}
	
	private int createAndSendFuAPacket(int sum, int len, int type) throws IOException {
		/* 
		    0                   1                   2                   3
		    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		   | FU indicator  |   FU header   |                               |
		   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+                               |
		   |                                                               |
		   |                         FU payload                            |
		   |                                                               |
		   |                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		   |                               :...OPTIONAL RTP padding        |
		   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		
		   Figure 14.  RTP payload format for FU-A
		 */
	
		/*
		   +---------------+ 
		   |0 1 2 3 4 5 6 7|
		   +-+-+-+-+-+-+-+-+
		   |F|NRI|  Type   |
		   +---------------+
		   
		   FU Indicator
	
	        F: 1 bit
	            forbidden_zero_bit.  The H.264 specification declares a value of
	            1 as a syntax violation.
	
	        NRI: 2 bits
	            nal_ref_idc.  A value of 00 indicates that the content of the NAL
	            unit is not used to reconstruct reference pictures for inter
	            picture prediction.  Such NAL units can be discarded without
	            risking the integrity of the reference pictures.  Values greater
	            than 00 indicate that the decoding of the NAL unit is required to
	            maintain the integrity of the reference pictures.
	
	        Type: 5 bits
	            nal_unit_type.  This component specifies the NAL unit payload type
	            as defined in table 7-1 of [1], and later within this memo.  For a
	            reference of all currently defined NAL unit types and their
	            semantics, please refer to section 7.4.1 in [1].
	       
		 */
		
		/* 	0x60 = 0110 0000 
		  
		  	nal_ref_idc
		  
			Values greater than 00 indicate that the
			decoding of the NAL unit is required to maintain the integrity of
			the reference pictures.
			
			In addition to the specification above, according to this RTP
			payload specification, values of NRI greater than 00 indicate the
			relative transport priority, as determined by the encoder. (RFC 3984)
		 */
		mBuffer[mRtphl + 0] = (byte)((mBuffer[mRtphl] & 0x60) * 0xFF);
		//mBuffer[mRtphl + 0] = (byte)((mBuffer[mRtphl] & 0x60) >> 5);
		mBuffer[mRtphl + 0] += 28;		// NALU payload type FU-A type for FU Indicator
		
		/*
				The FU header has the following format:	
					      
		      +---------------+
		      |0|1|2|3|4|5|6|7|
		      +-+-+-+-+-+-+-+-+
		      |S|E|R|  Type   |
		      +---------------+
		 */
	
		/* 0x1F = 0001 1111 */
		mBuffer[mRtphl + 1] = (byte)(mBuffer[mRtphl] & 0x1F);	// Type from FU Header
		/* 	0x80 = 1000 0000 
		 
		 	Start bit for FU Header
		 	
			When set to one, the Start bit indicates the start of a fragmented
			NAL unit.  When the following FU payload is not the start of a
			fragmented NAL unit payload, the Start bit is set to zero. (RFC 3984)
		 */
		mBuffer[mRtphl + 1] += 0x80; 						
		
		while (sum < mNaluLength) {
			int fillLen = 0;
			if (mNaluLength - sum > MAX_PKT_SIZE - mRtphl - FU_A_HEADER_SIZE)
				fillLen = MAX_PKT_SIZE - mRtphl - FU_A_HEADER_SIZE;
			else
				fillLen = mNaluLength - sum;
				
			if ((len = fillBuffer(mRtphl + FU_A_HEADER_SIZE, fillLen)) < 0)
				return -1;
			sum += len;
			
			// last pkt before next NAL
			if (sum >= mNaluLength) {
				/* 	0x40 = 0100 0000 
				 
				 	End bit for FU Header
				 	
					When set to one, the End bit indicates the end of a fragmented NAL
					unit, i.e., the last byte of the payload is also the last byte of
					the fragmented NAL unit.  When the following FU payload is not the
					last fragment of a fragmented NAL unit, the End bit is set to
					zero. (RFC 3984)
				 */
				mBuffer[mRtphl + 1] += 0x40;
				mSock.setNextPacketRTPMarker();
			}
			
			mSock.send(len + mRtphl + FU_A_HEADER_SIZE);
		}
		return len;
	}
	
	private int createSingleNaluPacket(int sum, int len, int type) throws IOException {
		/* 			 
			 0                   1                   2                   3
			 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
			+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
			|F|NRI|  Type   |                                               |
			+-+-+-+-+-+-+-+-+                                               |
			|                                                               |
			|               Bytes 2..n of a single NAL unit                 |
			|                                                               |
			|                               +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
			|                               :...OPTIONAL RTP padding        |
			+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
			
			Figure 2.  RTP payload format for single NAL unit packet
			(RFC 6184)
		 
		 */
		
		   /* skip that 1 byte of FU indicator */
		len = fillBuffer(mRtphl + 1, mNaluLength -1);
		mSock.setNextPacketRTPMarker();	
		return len;
	}
	
	public void send() throws IOException {
		int sum = 1;
		int len = 0;
		int type = 0;
		
		/* MDAT atom format
		 
		 	"The MP4 container format stores H.264 data without start codes. 
		 	Instead, each NALU is prefixed by a length field, which gives 
		 	the length of the NALU in bytes. The size of the length field 
		 	can vary, but is typically 1, 2, or 4 bytes."
		 
		 	http://msdn.microsoft.com/en-us/library/windows/desktop/dd757808%28v=vs.85%29.aspx
		 	
		 	Hex editor suggests 4 bytes. Internet people's hex editors, as well.
		 	So we went with 4 bytes.
		 	
		 	The typical structure appears to be as follows:
		 		
		 		[MDAT][LENGTH_{1}][DATA_{1}][LENGTH_{2}][DATA_{2}]...[LENGTH_{N}][DATA_{N}]
		 */
		fillBuffer(mRtphl, 4);
		mNaluLength = (mBuffer[mRtphl] & 0xFF) << 24
					| (mBuffer[mRtphl + 1] & 0xFF) << 16
					| (mBuffer[mRtphl + 2] & 0xFF) << 8
					| (mBuffer[mRtphl +3] & 0xFF);
		
		/* NAL Unit header (1B)
		 
			All NAL units consist of a single NAL unit type octet, which also
			co-serves as the payload header of this RTP payload format.  The
			payload of a NAL unit follows immediately. (RFC 3984)
		 */
		fillBuffer(mRtphl, 1);
		
		mTimestamp += mDelay;
		/*
		    The RTP timestamp is set to the sampling timestamp of the content.
		    A 90 kHz clock rate MUST be used. (RFC 3984)
		 */
		mSock.updateTimestamp(mTimestamp * 90);	
		
		/* Can we fit in a single packet? */
		if (mNaluLength <= MAX_PKT_SIZE - mRtphl - FU_A_HEADER_SIZE) {
			
			len = createSingleNaluPacket(sum, len, type);
			mSock.send(mNaluLength + mRtphl);
			
			//Log.e(TAG, "h264pktzr sent single NALU");
		} else {
				len = createAndSendFuAPacket(sum, len, type);
				if (len == -1)
					return;
				
				//Log.e(TAG, "h264pktzr sent FU-A NALU");
				
				// toggle start bit
				mBuffer[mRtphl + 1] = (byte)(mBuffer[mRtphl + 1] & 0x7F);
		}
	}
}
